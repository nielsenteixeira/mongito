﻿using Mongito.DataTranslator;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mongito.Collector
{
    public interface ISQLCollector<T>: IMongitoCollector<T> where T : ITranslatable
    {
    }
}
