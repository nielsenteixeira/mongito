﻿using Mongito.DataTranslator;
using System.Collections.Generic;

namespace Mongito.Collector
{
    public interface IMongitoCollector<T> where T : ITranslatable
    {
        IEnumerable<T> Collect();
    }
}
