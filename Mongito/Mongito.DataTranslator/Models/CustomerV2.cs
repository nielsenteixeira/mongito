﻿using System;
using System.Collections.Generic;

namespace Mongito.DataTranslator.Models
{
    public class CustomerV2
    {
        public int IdCustomer { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string IdCard { get; set; }
        public Address Address { get; set; }
        public IEnumerable<Purchase> Purchases { get; set; }
        public IEnumerable<CreditCard> CreditCards { get; set; }
        public Address DeliveryAddress { get; set; }
    }
}
