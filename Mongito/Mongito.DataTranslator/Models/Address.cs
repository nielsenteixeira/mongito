﻿namespace Mongito.DataTranslator.Models
{
    public class Address
    {
        public string Street { get; set; }
        public int HouseNumber { get; set; }
        public string Neighborhood { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
    }
}