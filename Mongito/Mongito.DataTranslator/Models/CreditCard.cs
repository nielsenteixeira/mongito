﻿using System;

namespace Mongito.DataTranslator.Models
{
    public class CreditCard
    {
        public string Number { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string CVV { get; set; }
    }
}