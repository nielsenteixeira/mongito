﻿using System;

namespace Mongito.DataTranslator.Models
{
    public class Customer: ITranslatable
    {
        public int IdCustomer { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string IdCard { get; set; }
        public string Street { get; set; }

        public int HouseNumber { get; set; }
        public string Neighborhood { get; set; }
        public string ZipCode { get; set; }
        public string City{ get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public string PurchaseItemName { get; set; }
        public float PurchasePrice { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string PurchaseFormOfPayment { get; set; }

        public string CreditCardNumber { get; set; }
        public DateTime CreditCardExpirationDate { get; set; }
        public string CreditCardCVV { get; set; }
        public string DeliveryAddress { get; set; }

        public string DeliveryAddressStreet { get; set; }
        public int DeliveryAddressHouseNumber { get; set; }
        public string DeliveryAddressNeighborhood { get; set; }
        public string DeliveryAddressZipCode { get; set; }
        public string DeliveryAddressCity { get; set; }
        public string DeliveryAddressState { get; set; }
        public string DeliveryAddressCountry { get; set; }

        public string PurchaseEvaluation { get; set; }
        public int PurchaseGrade { get; set; }
    }
}
