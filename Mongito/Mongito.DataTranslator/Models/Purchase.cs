﻿using System;

namespace Mongito.DataTranslator.Models
{
    public class Purchase
    {
        public string ItemName { get; set; }
        public float Price { get; set; }
        public DateTime Date { get; set; }
        public string FormOfPayment { get; set; }

        public string PurchaseEvaluation { get; set; }
        public int PurchaseGrade { get; set; }
    }
}